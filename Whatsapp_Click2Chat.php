<?php
/*
Plugin Name: Whatsapp Click 2 Chat
Description: Enables WhatsApp chat buttons in your web pages.
Version: 0.1
Author: Sakil Alam
Author URI: https://fb.me/DeejaySKL
License: GPLv2
*/
define('WHATSAPP_DEFAULT_IMG', '<svg width="48px" height="48px" class="nta-whatsapp-default-avatar" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
    <path style="fill:#EDEDED;" d="M0,512l35.31-128C12.359,344.276,0,300.138,0,254.234C0,114.759,114.759,0,255.117,0
    S512,114.759,512,254.234S395.476,512,255.117,512c-44.138,0-86.51-14.124-124.469-35.31L0,512z"/>
    <path style="fill:#55CD6C;" d="M137.71,430.786l7.945,4.414c32.662,20.303,70.621,32.662,110.345,32.662
    c115.641,0,211.862-96.221,211.862-213.628S371.641,44.138,255.117,44.138S44.138,137.71,44.138,254.234
    c0,40.607,11.476,80.331,32.662,113.876l5.297,7.945l-20.303,74.152L137.71,430.786z"/>
    <path style="fill:#FEFEFE;" d="M187.145,135.945l-16.772-0.883c-5.297,0-10.593,1.766-14.124,5.297
    c-7.945,7.062-21.186,20.303-24.717,37.959c-6.179,26.483,3.531,58.262,26.483,90.041s67.09,82.979,144.772,105.048
    c24.717,7.062,44.138,2.648,60.028-7.062c12.359-7.945,20.303-20.303,22.952-33.545l2.648-12.359
    c0.883-3.531-0.883-7.945-4.414-9.71l-55.614-25.6c-3.531-1.766-7.945-0.883-10.593,2.648l-22.069,28.248
    c-1.766,1.766-4.414,2.648-7.062,1.766c-15.007-5.297-65.324-26.483-92.69-79.448c-0.883-2.648-0.883-5.297,0.883-7.062
    l21.186-23.834c1.766-2.648,2.648-6.179,1.766-8.828l-25.6-57.379C193.324,138.593,190.676,135.945,187.145,135.945"/>
</svg>');

function whatsapp_chat_shortcode($id)
{
        extract($id);
        $check_account_status = get_post_status($id);
        if ($check_account_status == false || $check_account_status != 'publish') {
            return '';
        }
        $data_posts = get_the_title($id);
        $data_meta = get_post_meta($id, 'whatsapp_accounts', true);
        $online_status = $data_meta['online'] == 'checked';
        $avatar = get_the_post_thumbnail_url($id);
        $content = $href = '';

        if (wp_is_mobile()) {
            $url = 'href="https://api.whatsapp.com/send?phone=';
        }
        else
        	$url = 'target="_blank" href="https://web.whatsapp.com/send?phone=';

        if (strpos( $data_meta['wa_number'], 'chat.whatsapp.com')) {
            $href .= 'target="_blank" href="'.esc_url( $data_meta['wa_number'] ).'"';
        } else {
            $href .= $url;
            $number = preg_replace( '/[^0-9]/', '', $data_meta['wa_number'] );
            $href .= $number;
            $href .= '&text=Hi';
            $href .= '"';
        }

        $buttonSetting = get_option('wabutton_setting');
        $mainStyle = array();
        if (empty($buttonSetting)) {
            $mainStyle['button-text'] = 'Need help? Chat via Whatsapp';
            $mainStyle['button_back_color'] = '#2DB742';
            $mainStyle['button_text_color'] = '#fff';
        } else {
            if ($buttonSetting['button-text'] == '') {
                $mainStyle['button-text'] = 'Need help? Chat via Whatsapp';
            } else {
                $mainStyle['button-text'] = $buttonSetting['button-text'];
            }
            $mainStyle['button_back_color'] = $buttonSetting['button_back_color'];
            $mainStyle['button_text_color'] = $buttonSetting['button_text_color'];
        }
        wp_register_style('nta-wabutton-style', plugins_url( '/assets/css/style.css', __FILE__ ));
        wp_enqueue_style('nta-wabutton-style');
        $custom_css = "
                #nta-wabutton-$id .wa__stt_online{
                        background: " . $mainStyle['button_back_color'] . ";
                }

                #nta-wabutton-$id .wa__stt_online{
                        color: " . $mainStyle['button_text_color'] . ";
                }

                #nta-wabutton-$id .wa__stt_online .wa__cs_name{
                        color: " . $mainStyle['button_text_color'] . ";
                        opacity: 0.8;
                }

                #nta-wabutton-$id p{
                        display: none;
                }
                ";
        wp_add_inline_style('nta-wabutton-style', $custom_css);

        if ($avatar == false) {
            if ($online_status) {
                $content = '<div id="nta-wabutton-' . $id . '" style="margin: 30px 0 30px;">';
                $content .= '<a ' . $href;
                $content .= ' class="wa__button wa__r_button wa__stt_online wa__btn_w_icon ' . (empty($data_posts) ? 'wa__button_text_only' : '') . '">';
                $content .= '<div class="wa__btn_icon">';
                $content .= '<img src="' . plugins_url('',__FILE__) . '/assets/img/whatsapp_logo.svg" alt="img"/></div>';
                $content .= '<div class="wa__btn_txt">';
                if (!empty($data_posts)) {
                    $content .= '<div class="wa__cs_info"><div class="wa__cs_name">' . $data_posts . '</div>';
                    $content .= '<div class="wa__cs_status">Online</div></div>';
                }
                $content .= '<div class="wa__btn_title">' . $mainStyle['button-text'] . '</div>';
                $content .= '</div></a></div>';
            } else {
                $content = '<div id="nta-wabutton-' . $id . '" style="margin: 30px 0 30px;">
                <div class="wa__button wa__r_button wa__stt_offline  wa__btn_w_icon ' . (empty($data_posts) ? 'wa__button_text_only_me' : '') . '">';
                $content .= '<div class="wa__btn_icon">';
                $content .= '<img src="' . plugins_url('',__FILE__) . '/assets/img/whatsapp_logo_gray.svg" alt=""/></div>';
                $content .= '<div class="wa__btn_txt">';
                if (!empty($data_posts)) {
                    $content .= '<div class="wa__cs_info"><div class="wa__cs_name">' . $data_posts . '</div>';
                    $content .= '<div class="wa__cs_status">Offline</div></div>';
                }
                $content .= '<div class="wa__btn_title">' . $mainStyle['button-text'] . '</div>';
                $content .= '<div class="wa__btn_status">I will be back soon</div></div></div></div>';
            }
                
        } else {
            if ($online_status) {
                $content = '<div id="nta-wabutton-' . $id . '" style="margin: 30px 0 30px;">';
                $content .= '<a ' . $href;
                $content .= ' class="wa__button wa__r_button wa__stt_online wa__btn_w_img ' . (empty($data_posts) ? 'wa__button_text_only' : '') . '">';
                $content .= '<div class="wa__cs_img">';
                $content .= '<div class="wa__cs_img_wrap" style="background: url(' . $avatar . ') center center no-repeat; background-size: cover;"></div></div>';
                $content .= '<div class="wa__btn_txt">';
                if (!empty($data_posts)) {
                    $content .= '<div class="wa__cs_info"><div class="wa__cs_name">' . $data_posts . '</div>';
                    $content .= '<div class="wa__cs_status">Online</div></div>';
                }

                $content .= '<div class="wa__btn_title">' . $mainStyle['button-text'] . '</div>';
                $content .= '</div></a></div>';
            } else {
                $content = '<div id="nta-wabutton-' . $id . '" style="margin: 30px 0 30px;">
                <div class="wa__button wa__r_button wa__stt_offline wa__btn_w_img ' . (empty($data_posts) ? 'wa__button_text_only_me' : '') . '">';
                $content .= '<div class="wa__cs_img">';
                $content .= '<div class="wa__cs_img_wrap" style="background: url(' . $avatar . ') center center no-repeat; background-size: cover;"></div></div>';
                $content .= '<div class="wa__btn_txt">';
                if (!empty($data_posts)) {
                    $content .= '<div class="wa__cs_info"><div class="wa__cs_name">' . $data_posts . '</div>';
                    $content .= '<div class="wa__cs_status">Offline</div></div>';
                }
                $content .= '<div class="wa__btn_title">' . $mainStyle['button-text'] . '</div>';
                $content .= '<div class="wa__btn_status">I will be back soon</div></div></div></div>';
            }
        }

        return $content;
}

add_shortcode( 'Click2Chat', 'whatsapp_chat_shortcode' );
function load_style()
{
    wp_register_script('nta-js-popup', plugins_url( '/assets/js/main.js', __FILE__ ), ['jquery']);
    wp_enqueue_script('nta-js-popup');
}
add_action( 'wp_enqueue_scripts', 'load_style' );
$widget_setting = get_option('wabutton_setting');
//Check available account and setting to show widget
if (isset($widget_setting['floating_widget_status']) && $widget_setting['floating_widget_status'] == 'ON') {
    add_action('wp_footer', 'show_popup_view');
}

if (isset($widget_setting['woo_button_status']) && $widget_setting['woo_button_status'] == 'ON') {
    if ($widget_setting['woo_button_position'] == 'after_atc') {
        add_action('woocommerce_after_add_to_cart_button', 'insert_wa_woobutton');
    } elseif ($widget_setting['woo_button_position'] == 'before_atc') {
        add_action('woocommerce_before_add_to_cart_button', 'insert_wa_woobutton');
    } elseif ($widget_setting['woo_button_position'] == 'after_short_description') {
        add_filter('woocommerce_short_description', 'showAfterShortDescription');
    } elseif ($widget_setting['woo_button_position'] == 'after_long_description') {
        add_filter('the_content', 'showAfterLongDescription');
    }
}

add_action('wp_head', 'popup_style_setting');

function insert_wa_woobutton() {
    $query = new WP_Query('post_type=wa-accounts');
    $account_list = $query->posts;
    $account_list_view = array();
    foreach ($account_list as $account) {
        $get_data = get_post_meta($account->ID, 'whatsapp_accounts', true);

        $account_list_view[$account->ID] = array(
            'account_id' => $account->ID,
        );
    }

    foreach ($account_list_view as $row) {
        echo '<div class="nta-woo-products-button">' . do_shortcode('[Click2Chat id="' . $row['account_id'] . '"]') . '</div>';
    }
}

function showAfterShortDescription($post_excerpt) {
    if (!is_single()) {
        return $post_excerpt;
    }
    $btn_content = '';
    $query = new WP_Query('post_type=wa-accounts');
    $account_list = $query->posts;
    $account_list_view = array();
    foreach ($account_list as $account) {
        $get_data = get_post_meta($account->ID, 'whatsapp_accounts', true);

        $account_list_view[$account->ID] = array(
            'account_id' => $account->ID,
        );
    }

    foreach ($account_list_view as $row) {
        $btn_content .= '<div class="nta-woo-products-button">' . do_shortcode('[Click2Chat id="' . $row['account_id'] . '"]') . '</div>';
    }
    return $post_excerpt . $btn_content;
}

function showAfterLongDescription($content) {
    if ('product' !== get_post_type() || !is_single()) {
        return $content;
    }
    $btn_content = '';
    $query = new WP_Query('post_type=wa-accounts');
    $account_list = $query->posts;
    $account_list_view = array();
    foreach ($account_list as $account) {
        $get_data = get_post_meta($account->ID, 'whatsapp_accounts', true);

        $account_list_view[$account->ID] = array(
            'account_id' => $account->ID,
        );
    }

    foreach ($account_list_view as $row) {
        $btn_content .= '<div class="nta-woo-products-button">' . do_shortcode('[Click2Chat id="' . $row['account_id'] . '"]') . '</div>';
    }
    return $content . $btn_content;
}
function show_popup_view() {
    $option['widget_name'] = 'Start a Conversation';
    $option['widget_description'] = 'Hi! Click one of our member below to chat on <strong>Whatsapp</strong>';
    $option['widget_label'] = 'Need Help? <strong>Chat with us</strong>';
    $option['widget_responseText'] = 'The team typically replies in a few minutes.';

    //Show Account Data
    $query = new WP_Query('post_type=wa-accounts');
    $account_list = $query->posts;
    $account_list_view = array();
    foreach ($account_list as $account) {
        $get_data = get_post_meta($account->ID, 'whatsapp_accounts', true);

        $account_list_view[$account->ID] = array(
            'account_id' => $account->ID,
            'post_title' => $account->post_title,
            'wa_number' => $get_data['wa_number'],
            'wa_title' => $get_data['wa_title'],
            'online' => $get_data['online']=='checked',
            'avatar' => get_the_post_thumbnail_url($account->ID)
        );
    }

    require(plugin_dir_path(__FILE__).'views/whatsapp-widget-view.php');
}
function popup_style_setting() {
    $option['text_color'] = '#fff';
    $option['back_color'] = '#2db742';
    $option['widget_position'] = 'right';
    ?>
    <style>
        .wa__stt_offline{
            pointer-events: none;
        }

        .wa__button_text_only_me .wa__btn_txt{
            padding-top: 16px !important;
            padding-bottom: 15px !important;
        }

        .wa__popup_content_item .wa__cs_img_wrap{
            width: 48px;
            height: 48px;
        }

        .wa__popup_chat_box .wa__popup_heading{
            background: <?php echo $option['back_color'] ?>;
        }

        .wa__btn_popup .wa__btn_popup_icon{
            background: <?php echo $option['back_color'] ?>;
        }

        .wa__popup_chat_box .wa__stt{
            border-left: 2px solid  <?php echo $option['back_color'] ?>;
        }

        .wa__popup_chat_box .wa__popup_heading .wa__popup_title{
            color: <?php echo $option['text_color'] ?>;
        }

        .wa__popup_chat_box .wa__popup_heading .wa__popup_intro{
            color: <?php echo $option['text_color'] ?>;
            opacity: 0.8;
        }

        .wa__popup_chat_box .wa__popup_heading .wa__popup_intro strong{

        }

        <?php if ($option['widget_position'] == 'left'): ?>
            .wa__btn_popup{
                left: 30px;
                right: unset;
            }

            .wa__btn_popup .wa__btn_popup_txt{
                left: 100%;
            }

            .wa__popup_chat_box{
                left: 25px;
            }
        <?php endif; ?>

    </style>

    <?php
}
add_action('admin_init', function() {
	register_setting('wa_whatsapp_button_group', 'wabutton_setting', function ($input) {
        $new_input = [];
        $new_input['button-text'] = $_POST['button-text'];
        $new_input['button_back_color'] = $_POST['button_back_color'];
        $new_input['button_text_color'] = $_POST['button_text_color'];
        if (isset($_POST['floating_widget_status'])) {
            $new_input['floating_widget_status'] = 'ON';
        }
        $new_input['woo_button_position'] = $_POST['woo_button_position'];
        if (isset($_POST['woo_button_status'])) {
            $new_input['woo_button_status'] = 'ON';
        }
        return $new_input;
    });
	add_settings_section('wa_page_settings', '', function ()
	{
		$option_group = 'wa_whatsapp_button_group';
	    $option = get_option('wabutton_setting');
	    if (!$option) {
	        $option = array();
	        $option['button-text'] = 'Need help? Chat via Whatsapp';
	        $option['button_back_color'] = '#2db742';
	        $option['button_text_color'] = '#fff';
            $option['floating_widget_status'] = 'ON';
	    }
	    require (plugin_dir_path(__FILE__).'views/settings-page.php');
	}, 'wa_settings-page');
});
add_action('admin_menu', 'wa_menu_page');
add_action( 'admin_enqueue_scripts', function() {
            wp_register_style('nta-css', plugins_url('scripts/css/style.css', __FILE__ ));
            wp_enqueue_style('nta-css');
            wp_enqueue_style('wp-color-picker');

            wp_register_script('nta-js', plugins_url('scripts/js/admin-scripts.js', __FILE__ ), ['jquery', 'wp-color-picker']);
            wp_localize_script('nta-js', 'nta', [
                'url' => admin_url('admin-ajax.php')
            ]);

            wp_enqueue_script('nta-js-sortable', plugins_url('scripts/js/jquery-ui.min.js', __FILE__ ));
            wp_enqueue_script('nta-js');
        });
function wa_menu_page() {
	$edit_account_link = 'post-new.php?post_type=wa-accounts';
	add_menu_page('WhatsApp', 'WhatsApp', 'manage_options', 'wa_page', 'create_page_setting');
	add_submenu_page('wa_page', 'Add New account', 'Add New account', 'edit_posts', $edit_account_link);
	add_submenu_page('wa_page', 'Settings', 'Settings', 'manage_options', 'wa_page', 'create_page_setting');
}
function create_page_setting() {
    ?> 
    <div class="wrap">
        <h1>Settings</h1>

        <?php settings_errors(); ?> 
 
        <div class="nta-tabs-content">
            <form name="post" method="post" action="options.php" id="post" autocomplete="off"> 
                <?php
                	do_settings_sections('wa_settings-page');
                ?>
            </form>
        </div>
    </div>
    <?php
}


add_action('init', function() {

    $labels = array(
        'name' => __('WhatsApp Accounts'),
        'singular_name' => __('Whatsapp Account'),
        'add_new' => __('Add  New Account'),
        'add_new_item' => __('Add New Account'),
        'edit_item' => __('Edit Account'),
        'new_item' => __('New Account'),
        'all_items' => __('All Accounts'),
        'view_item' => __('View Accounts'),
        'search_items' => __('Search Account'),
        'featured_image' => 'Avatar',
        'set_featured_image' => 'Select an image',
        'remove_featured_image' => 'Remove avatar'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Manager Accounts',
        'public' => false,
        'show_ui' => true,
        'has_archive' => true,
        'show_in_admin_bar' => false,
        'show_in_menu' => 'wa_page',
        'menu_position' => 100,
        'query_var' => 'wa-accounts',
        'supports' => array(
            'title',
            'thumbnail',
        ),
    );
    register_post_type('wa-accounts', $args);
});
add_action('save_post', 'save_account', 10, 2);
add_action('add_meta_boxes', function() {
    add_meta_box('whatsapp-account-info', 'WhatsApp Account Information', 'meta_form_account', 'wa-accounts', 'normal');
    $current_screen = get_current_screen();
    if ($current_screen->action != 'add') {
        add_meta_box('whatsapp-button-shortcode', 'Shortcode for this account', 'account_shortcode_form', 'wa-accounts', 'side');
    }
});
add_filter('manage_wa-accounts_posts_columns', 'manager_accounts_columns', 10, 1);
add_action('manage_wa-accounts_posts_custom_column', 'manager_accounts_show_columns', 10, 2);
function save_account($post_id, $post) {
    // Kiểm tra nếu nonce chưa được gán giá trị
    if (!isset($_POST['form_account_nonce'])) {
        return;
    }
    // Kiểm tra nếu giá trị nonce không trùng khớp
    if (!wp_verify_nonce($_POST['form_account_nonce'], 'save_form_account')) {
        return;
    }

    $new_account = array(
        'wa_number' => $_POST['wa_number'],
        'wa_title' => $_POST['wa_title'],
        'online' => isset($_POST['online']) ? 'checked' : ''
    );

    $refer_url = $_POST['_wp_http_referer'];
    $add_new_action = strpos($refer_url, 'post-new.php');

    update_post_meta($post_id, 'whatsapp_accounts', $new_account);
}
function meta_form_account($post) {
    wp_nonce_field('save_form_account', 'form_account_nonce');
    $edit_account = get_post_meta($post->ID, 'whatsapp_accounts', true);
    require(plugin_dir_path(__FILE__).'views/whatsapp-meta-accounts.php');
}
function account_shortcode_form() {
    ?>
    <p>Copy the shortcode below and paste it into the editor to display the button.</p>
    <p><input type="text" id="nta-button-shortcode-copy" value="[Click2Chat id=&quot;<?php echo get_the_ID() ?>&quot;]" class="widefat" readonly=""></p>
    <p class="nta-shortcode-copy-status hidden" style="color: green"><strong>Copied!</strong></p>
    <?php
}
function manager_accounts_columns($columns) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => 'Account Name',
        'wa_avatar' => 'Avatar',
        'wa_number' => 'Number',
        'wa_title' => 'Title',
        'shortcode' => 'Shortcode'
    );
    return $columns;
}
function manager_accounts_show_columns($name, $post_id) {
    $data_account = get_post_meta($post_id, 'whatsapp_accounts', true);

    switch ($name) {
        case 'wa_avatar':
            the_post_thumbnail('thumbnail', array('class' => 'img-size-table'));
            break;
        case 'wa_number':
            echo $data_account['wa_number'];
            break;
        case 'wa_title':
            echo $data_account['wa_title'];
            break;
        case 'shortcode':
            echo '<input type="text" class="nta-shortcode-table" name="country" value="[Click2Chat id=&quot;' . $post_id . '&quot;]" readonly>';
            break;
    }
}
?>