<input type="hidden" name="option_page" value="<?php echo $option_group; ?>">
<input type="hidden" name="action" value="update">
<?php wp_nonce_field($option_group . '-options'); ?>
<table class="form-table">
    <p>Use this form to set default style for shortcode buttons.</p>
    <tbody>
        <tr>
            <th scope="row"><label for="nta-whatsapp-button-text">Default Button Text</label></th>
            <td>
                <input type="text" id="nta-whatsapp-button-text" name="button-text" value="<?php echo $option['button-text'] ?>" class="nta-whatsapp-button-text regular-text" placeholder="Need help? Chat via Whatsapp"/>    
            </td>    
        </tr>

        <tr>
            <th scope="row"><label for="button_back_color">Button Background Color</label></th>
            <td>
                <input type="text" id="button_back_color" name="button_back_color" value="<?php echo $option['button_back_color'] ?>" class="widget-background-color" data-default-color="#2DB742" />    
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="button_text_color">Button Text Color</label></th>
            <td>
                <input type="text" id="button_text_color" name="button_text_color" value="<?php echo $option['button_text_color'] ?>" class="widget-background-color" data-default-color="#fff" />    
            </td>
        </tr>
    </tbody>
</table>
<hr/>
<table class="form-table">
    <p>Settings for the floating widget.</p>
    <tbody>
        <tr>
            <th scope="row"><label for="nta-wa-switch-control">Enabled</label></th>
            <td>
                <div class="nta-wa-switch-control">
                    <input type="checkbox" class="nta-wa-switch" id="switch-1" name="floating_widget_status" <?php echo (isset($option['floating_widget_status']) ? 'checked' : '') ?>>
                    <label for="switch-1" class="green"></label>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<hr/>
<table class="form-table">
    <p>Settings for displaying buttons on WooCommerce product page.</p>
    <tbody>
        <tr>
            <th scope="row"><label for="nta-wa-switch-control">Enabled</label></th>
            <td>
                <div class="nta-wa-switch-control">
                    <input type="checkbox" class="nta-wa-switch" id="switch-2" name="woo_button_status" <?php echo (isset($option['woo_button_status']) ? 'checked' : '') ?>>
                    <label for="switch-2" class="green"></label>
                </div>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="nta_woo_button_position">Button position</label></th>
            <td>
                <select name="woo_button_position" id="nta_woo_button_position">
                    <option value="before_atc" <?php echo ($option['woo_button_position'] == 'before_atc' ? 'selected' : '') ?>>Before Add to Cart button</option>
                    <option value="after_atc" <?php echo ($option['woo_button_position'] == 'after_atc' ? 'selected' : '') ?>>After Add to Cart button</option>
                    <option value="after_short_description" <?php echo ($option['woo_button_position'] == 'after_short_description' ? 'selected' : '') ?>>After short description</option>
                    <option value="after_long_description" <?php echo ($option['woo_button_position'] == 'after_long_description' ? 'selected' : '') ?>>After long description</option>
                </select>
            </td>
        </tr>
    </tbody>
</table>
<button class="button button-primary button-large" id="btnSave" type="submit">Save Changes</button>