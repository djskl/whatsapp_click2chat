<table class="form-table" id="nta-custom-wc-button-settings">
    <tbody>
        <tr>
            <th scope="row">
                <label for="wa_number">
                    WhatsApp Number
                </label>
            </th>
            <td>
                <p>
                    <input type="text" class="widefat" id="wa_number" name="wa_number" value="<?php echo (!empty($edit_account) ? $edit_account['wa_number'] : '') ?>" autocomplete="off">
                </p>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <label for="wa_title">Title</label>
            </th>
            <td>
                <input type="text" id="wa_title" name="wa_title" value="<?php echo (!empty($edit_account) ? $edit_account['wa_title'] : '') ?>" class="widefat" autocomplete="off">
            </td>
        </tr>
        <tr>
            <th scope="row"><label>Status</label></th>
            <td>
                <input type="checkbox" id="online" name="online" <?php echo (!empty($edit_account) ? $edit_account['online'] : '') ?>>
                <label for="online">Online</label>
            </td>
        </tr>
    </tbody>
</table>